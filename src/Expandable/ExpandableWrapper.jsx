import * as React from "react";
import "./Expandable.css";

export const ExpandableWrapper = ({onClickHandler, buttonText, openIcon, closeIcon, expandableContainerRef, open, children }) => {
    return (
        <div className="container">
        <button className="button" onClick={onClickHandler}>
            {buttonText}
            {!!open ? openIcon : closeIcon}
        </button>
        <div ref={expandableContainerRef} className="body">
            {children}
        </div>
        </div>
    );
}

export default ExpandableWrapper;
