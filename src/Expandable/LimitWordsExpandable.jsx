import * as React from "react";
import ExpandableWrapper from "./ExpandableWrapper";

export const LimitWordsExpandable = ({
    text,
    wordsSize,
    onClickHandler,
    buttonText,
    openIcon,
    closeIcon,
    expandableContainerRef,
    open
}) => {
    const truncatedText = text.split(" ").slice(0, wordsSize).join(" ");

    return (
      <ExpandableWrapper
        onClickHandler={onClickHandler}
        buttonText={buttonText}
        openIcon={openIcon}
        closeIcon={closeIcon}
        expandableContainerRef={expandableContainerRef}
        open={open}
      >
        {!!open ? text : `${truncatedText}...` }
      </ExpandableWrapper>
    );
}