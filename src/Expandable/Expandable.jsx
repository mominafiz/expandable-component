import * as React from "react";
import { useState, useRef, useEffect } from "react";
import { Icon } from "semantic-ui-react";
import ExpandableWrapper from "./ExpandableWrapper";
import "./Expandable.css";
import { LimitWordsExpandable } from "./LimitWordsExpandable";

const Expandable = ({
  isOpen,
  children,
  buttonText,
  variant,
  wordsSize,
  rowsSize,
  text,
  defaultBodyHeight,
  openIcon = <Icon color="blue" name="angle up" />,
  closeIcon = <Icon color="blue" name="angle down" />
}) => {
  const [open, setOpen] = useState(isOpen);
  const expandableContainerRef = useRef();
  const onClickHandler = () => {
    setOpen(!open);
  }

  useEffect(() => {
    if (expandableContainerRef && expandableContainerRef.current) {
      if (open) {
        expandableContainerRef.current.style.maxHeight = expandableContainerRef.current.scrollHeight + "px";
      } else {
        if (!variant) {
          expandableContainerRef.current.style.maxHeight = defaultBodyHeight || 0;
        } else {
          expandableContainerRef.current.style.maxHeight = defaultBodyHeight || expandableContainerRef.current.scrollHeight + "px";
        }
      }
    }
  }, [open, expandableContainerRef, variant, defaultBodyHeight])

  if (variant === "limitByWords") {
    return <LimitWordsExpandable
        onClickHandler={onClickHandler}
        buttonText={buttonText}
        openIcon={openIcon}
        closeIcon={closeIcon}
        expandableContainerRef={expandableContainerRef}
        open={open}
        wordsSize={wordsSize}
        text={text}
      />;
  }

  if (variant === "limitByRows") {
    const truncatedRows = React.Children.toArray(children).splice(0, rowsSize);
    return (
      <ExpandableWrapper
        onClickHandler={onClickHandler}
        buttonText={buttonText}
        openIcon={openIcon}
        closeIcon={closeIcon}
        expandableContainerRef={expandableContainerRef}
        open={open}
      >
        {!!open ? children : truncatedRows }
      </ExpandableWrapper>
    );
  }

  return (
    <ExpandableWrapper
        onClickHandler={onClickHandler}
        buttonText={buttonText}
        openIcon={openIcon}
        closeIcon={closeIcon}
        expandableContainerRef={expandableContainerRef}
        open={open}
      >
        {children}
      </ExpandableWrapper>
  );
};

export default Expandable;
