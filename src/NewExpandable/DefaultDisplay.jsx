import * as React from "react";

export const DefaultDisplay = React.forwardRef(
  ({ className, children }, ref) => (
    <div ref={ref} className={className}>
      {children}
    </div>
  )
);

export default DefaultDisplay;
