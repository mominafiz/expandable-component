import * as React from "react";
// import { useEffect } from "react";

export const DisplayRows = React.forwardRef(
  ({ children, rowsSize, className, open }, ref) => {
    return (
      <div className={className} ref={ref}>
        {open ? children : React.Children.toArray(children).splice(0, rowsSize)}
      </div>
    );
  }
);

export default DisplayRows;
