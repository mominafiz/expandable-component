import * as React from "react";

export const DisplayWords = React.forwardRef(
  ({ className, text, wordsSize, open }, ref) => {
    const truncatedText = text
      .split(" ")
      .splice(0, wordsSize)
      .join(" ");

    return (
      <div className={className} ref={ref}>
        {open ? text : `${truncatedText}...`}
      </div>
    );
  }
);
