import * as React from "react";
import { useState, useRef, useEffect } from "react";
import { Icon } from "semantic-ui-react";
import "./NewExpandable";

export const NewExpandable = ({
  isOpen,
  children,
  buttonText,
  initialBodyHeight,
  openIcon = <Icon color="blue" name="angle up" />,
  closeIcon = <Icon color="blue" name="angle down" />
}) => {
  const [open, setOpen] = useState(isOpen);
  const expandableContainerRef = useRef();
  const onClickHandler = () => {
    setOpen(!open);
  };

  useEffect(() => {
    if (expandableContainerRef && expandableContainerRef.current) {
      if (open) {
        expandableContainerRef.current.style.maxHeight =
          expandableContainerRef.current.scrollHeight + "px";
      } else {
        expandableContainerRef.current.style.maxHeight =
        initialBodyHeight || expandableContainerRef.current.scrollHeight + "px";
      }
    }
  }, [open, expandableContainerRef, initialBodyHeight]);

  return (
    <div className="container">
      <button className="button" onClick={onClickHandler}>
        {buttonText}
        {!!open ? openIcon : closeIcon}
      </button>
      {React.Children.map(children, child =>
        React.cloneElement(child, {
          open,
          ref: expandableContainerRef,
          className: "body"
        })
      )}
    </div>
  );
};

export default NewExpandable;
