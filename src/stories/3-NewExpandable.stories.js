import * as React from "react";
import { NewExpandable } from "../NewExpandable";
import "semantic-ui-css/semantic.min.css";
import { DisplayWords } from "../NewExpandable/DisplayWords";
import { DefaultDisplay } from "../NewExpandable/DefaultDisplay";
import DisplayRows from "../NewExpandable/DisplayRows";

export default {
  title: "NewExpandable",
  component: NewExpandable
};

export const DefaultExpandable = () => (
  <div style={{ margin: "10px" }}>
    <NewExpandable buttonText="Show more" initialBodyHeight="0px">
      <DefaultDisplay>
        <div>
          <div>Body goes here</div>
          <div>Body goes here</div>
          <div>Body goes here</div>
          <div>Body goes here</div>
        </div>
      </DefaultDisplay>
    </NewExpandable>
  </div>
);

export const LimitWordsExpandable = () => (
  <div style={{ margin: "10px" }}>
    <NewExpandable buttonText="Show more">
      <DisplayWords
        text="The slice() method returns a shallow copy of a portion of an array into a new array object selected from begin to end (end not included) where begin and end represent the index of items in that array. The original array will not be modified."
        wordsSize={10}
      />
    </NewExpandable>
  </div>
);

export const LimitRowsExpandable = () => (
  <div style={{ margin: "10px" }}>
    <NewExpandable buttonText="Show more">
      <DisplayRows rowsSize={2}>
        <div>Body goes here</div>
        <div>Body goes here</div>
        <div>Body goes here</div>
        <div>Body goes here</div>
      </DisplayRows>
    </NewExpandable>
  </div>
);

// export const AnyBodyExpandable = () => (
//   <div style={{ margin: "10px" }}>
//     <NewExpandable buttonText="Show more" initialBodyHeight="0px">
//       <div>
//         <div>Body goes here...ayn</div>
//         <div>Body goes here</div>
//         <div>Body goes here</div>
//         <div>Body goes here</div>
//       </div>
//     </NewExpandable>
//   </div>
// );

