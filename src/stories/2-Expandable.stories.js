import * as React from "react";
import Expandable from "../Expandable/Expandable";
import 'semantic-ui-css/semantic.min.css';

export default {
    title: "Expandable",
    component: Expandable,
};

export const DefaultExpandable = () => (
    <div style={{ margin: "10px"}}>
        <Expandable buttonText="Show more">
            <div>
                <div>Body goes here</div>
                <div>Body goes here</div>
                <div>Body goes here</div>
                <div>Body goes here</div>
            </div>
        </Expandable>
    </div>
);

export const LimitWordsExpandable = () => (
    <div style={{ margin: "10px"}}>
        <Expandable
            buttonText="Show more"
            variant="limitByWords"
            wordsSize={10}
            text="The slice() method returns a shallow copy of a portion of an array into a new array object selected from begin to end (end not included) where begin and end represent the index of items in that array. The original array will not be modified.">
        </Expandable>
    </div>
);

export const LimitRowsExpandable = () => (
    <Expandable
        buttonText="Show more"
        variant="limitByRows"
        rowsSize={2}>
        <div>Body goes here..1</div>
        <div>Body goes here..2</div>
        <div>Body goes here..3</div>
        <div>Body goes here..4</div>
    </Expandable>
);

export const DefaultDisplayExpandable = () => (
    <Expandable
        buttonText="Show more"
        defaultBodyHeight="40px"
    >
        <h1>
            Expandable Component!
        </h1>
        <div>Body goes here..1</div>
        <div>Body goes here..2</div>
        <div>Body goes here..3</div>
        <div>Body goes here..4</div>
    </Expandable>
)